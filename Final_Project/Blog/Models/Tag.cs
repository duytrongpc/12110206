﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
      
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}