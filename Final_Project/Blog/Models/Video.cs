﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Video
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string urlLink { set; get; }
    }
}