﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
       
        public int AccountID { set; get; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        public string Passwword { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng")]
        public string Email { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Chỉ được nhập tối đa 100 ký tự",MinimumLength=0)]
        public string FistName { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Chỉ được nhập tối đa 100 ký tự",MinimumLength=0)]
        public string LastName { set; get; }
        public virtual ICollection<Post> Post{set;get;}
       
    }
}