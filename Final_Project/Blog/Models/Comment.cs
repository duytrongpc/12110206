﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        public int ID { set; get; }
       
      
        public string Body { set; get; }
     //  [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
       
        public int LastTime
       {
           get
           {
               return (DateTime.Now - DateCreated).Minutes;
           }
       }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}