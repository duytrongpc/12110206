﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Question
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [Display(Name = "Họ tên")]
        public string Name { set; get; }
        
        [Display(Name = "Tuổi")]
        public int Age { set; get; }
        [Display(Name = "Địa chỉ")]
        public string Address { set; get; }
        [Display(Name = "Thư điện tử")]
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng")]
        public string Email { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [Display(Name = "Nội dung")]
        public string Body { set; get; }

    }
}