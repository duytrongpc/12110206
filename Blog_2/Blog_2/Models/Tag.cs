﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
     
        public int ID { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Phải nhập từ 10 đến 100 ký tự",
          MinimumLength = 10)]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}