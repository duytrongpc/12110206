﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [Display(Name = "Tiêu đề")]
        public string Title { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [Display(Name = "Nội dung")]
        public string Body { set; get; }
        [Display(Name = "Ngày Đăng")]
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [DataType(DataType.DateTime)]
     
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
     
        public int UserProfileUserID { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}