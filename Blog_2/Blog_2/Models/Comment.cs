﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
       
        public int ID { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [StringLength(int.MaxValue, ErrorMessage = "Phải nhập tối thiểu 50 ký tự",
         MinimumLength = 50)]
        public string Body { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        public string Author { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}